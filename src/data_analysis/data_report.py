import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

import os
import loader

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

DATA = ['../../dataset/data_analysis']

BATCH_SIZE = 4

data = loader.DataGenerator(DATA, (120, 160), 1, batch_size=BATCH_SIZE, grayscale=True)

for batch in data:
    print(batch)

print("Number of images:", len(data) * BATCH_SIZE)
