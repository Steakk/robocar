import gym
from gym import spaces

import envs.client as client

import time

class AutocarEnv(gym.Env):
    """Custom Environment that follows gym interface"""
    metadata = {'render.modes': ['human']}

    def __init__(self, car_config):
        super(AutocarEnv, self).__init__()

        # Define action and observation space
        # They must be gym.spaces objects
        self.car_config = car_config
        self.car = client.UnitySimulationClient(car_config)

        # Example when using discrete actions:
        # self.action_space = spaces.Discrete(N_DISCRETE_ACTIONS)

        # # Example for using image as input:
        # self.observation_space = spaces.Box(low=0, high=255,
        #         shape=(HEIGHT, WIDTH, N_CHANNELS), dtype=np.uint8)

    def step(self, action):
        """ Execute one time step within the environment """
        self.car.send_action(action)

        data = self.car.get_data()

        return data

    def reset(self):
        """ Reset the state of the environment to an initial state """

        self.car.stop()

        time.sleep(0.1)

        self.car = client.UnitySimulationClient(self.car_config)

    def render(self, mode='human', close=False):
        """ Render the environment to the screen """

        print('Plop')
