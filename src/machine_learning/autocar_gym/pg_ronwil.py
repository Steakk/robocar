import envs.autocar_env as autocar_env

# car config:
car_config = {
    # 'host': 'sim.diyrobocars.fr',
    'host': 'localhost',
    #'host': '192.168.103.69',
    'port': 9091,

    'car_name': None,
    'font_size': None,
    'body_style': None,
    'body_rgb': None
}

env = autocar_env.AutocarEnv(car_config) 

for _ in range(10):
    for _ in range(100):
        data = env.step([0, 1])

        # controller.set_state(data)

        # action[0] = controller.get_steer()
        # action[1] = controller.get_throttle()

    env.reset()
