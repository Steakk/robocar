# Config file to store the training configuration variables

# Model to test new normalisation grayscale method: (im / 127.5) - 1

# DATASET
DATASET = [
    '../../../autocar_pytorch/robocar_pytorch/dataset/rrl1_constant_1',
    '../../../autocar_pytorch/robocar_pytorch/dataset/rrl1_constant_2',
    '../../../autocar_pytorch/robocar_pytorch/dataset/rrl1_constant_3',
    '../../../autocar_pytorch/robocar_pytorch/dataset/rrl1_constant_4',
    '../../../autocar_pytorch/robocar_pytorch/dataset/gen_road_const_1',
    '../../../autocar_pytorch/robocar_pytorch/dataset/gen_road_const_2',
    '../../../autocar_pytorch/robocar_pytorch/dataset/gen_road_const_3',
    '../../../autocar_pytorch/robocar_pytorch/dataset/gen_road_const_4',
    '../../../autocar_pytorch/robocar_pytorch/dataset/gen_road_const_5',
    '../../../autocar_pytorch/robocar_pytorch/dataset/gen_road_const_6'
    ]
BATCH_SIZE = 64

# MODEL TYPE
MODEL_TYPE = 'nvidia'

# IMAGE DIMENSION
IMAGE_DIMENSION = (120, 160, 1)

# GRAYSCALE
GRAYSCALE = True
